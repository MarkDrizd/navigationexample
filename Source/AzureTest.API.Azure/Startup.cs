using System.Data.Entity.Migrations;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using AzureTest.API.Azure;
using AzureTest.API.Azure.Services;
using Microsoft.Azure.Mobile.Server.Config;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace AzureTest.API.Azure
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<PushNotificationService>().AsImplementedInterfaces().SingleInstance();

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // For more information on Web API tracing, see http://go.microsoft.com/fwlink/?LinkId=620686 
            config.EnableSystemDiagnosticsTracing();

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            
            new MobileAppConfiguration()
                .UseDefaultConfiguration()
                .ApplyTo(config);

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling =
                Newtonsoft.Json.ReferenceLoopHandling.Serialize;
            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling =
                Newtonsoft.Json.PreserveReferencesHandling.Objects;

            app.UseWebApi(config);

            MigrateDB();
        }

        private void MigrateDB()
        {
            var migrator = new DbMigrator(new DAL.Migrations.Configuration());
            migrator.Update(); 
        }
    }
}