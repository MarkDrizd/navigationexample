﻿using System.Web.Http;
using System.Web.Http.Controllers;
using AzureTest.API.Azure.Services;
using AzureTest.DAL;
using Microsoft.Azure.Mobile.Server;

namespace AzureTest.API.Azure.Controllers.TableController
{
    [Authorize]
    public abstract class BaseTableController<T> : TableController<T> where T : EntityData
    {
        protected IPushNotificationService PushNotificationService { get; private set; }
        protected DatabaseContext DbContext;

        protected BaseTableController(IPushNotificationService pushNotificationService)
        {
            PushNotificationService = pushNotificationService;
        }

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            DbContext = new DatabaseContext();
            DomainManager = new EntityDomainManager<T>(DbContext, Request, true);
        }
    }
}