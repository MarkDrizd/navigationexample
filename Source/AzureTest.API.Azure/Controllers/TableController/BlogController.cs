﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using AzureTest.API.Azure.Services;
using AzureTest.DAL.Models;

namespace AzureTest.API.Azure.Controllers.TableController
{
    public class BlogController : BaseTableController<Blog>
    {
        // GET tables/Blog
        public IQueryable<Blog> GetAllUsers()
        {
            return Query();
        }

        // GET tables/Blog/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Blog> GetUser(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Blog/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Blog> PatchUser(string id, Delta<Blog> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/Blog
        public async Task<IHttpActionResult> PostUser(Blog item)
        {
            DbContext.Blogs.Add(item);
            await DbContext.SaveChangesAsync();
            return CreatedAtRoute("Tables", new { id = item.Id }, item);
        }

        // DELETE tables/Blog/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteUser(string id)
        {
            return DeleteAsync(id);
        }

        public BlogController(IPushNotificationService pushNotificationService) : base(pushNotificationService)
        {
        }
    }
}