﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using AzureTest.API.Azure.Services;
using AzureTest.DAL.Models;
using AzureTest.Model;

namespace AzureTest.API.Azure.Controllers.TableController
{
    public class UserController : BaseTableController<User>
    {
        // GET tables/User
        public IQueryable<User> GetAllUsers()
        {
            return Query();
        }

        // GET tables/User/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<User> GetUser(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/User/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<User> PatchUser(string id, Delta<User> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/User
        public async Task<IHttpActionResult> PostUser(User item)
        {
            DbContext.Users.Add(item);
            await DbContext.SaveChangesAsync();

            //TODO: Remove notifications, test purpose only 
            var pushNotification = new PushNotification
            {
                Message = "Notification test",
                AdditionalData = "Additional data"
            };

            await PushNotificationService.SendPushNotificationToAll(pushNotification);

            return CreatedAtRoute("Tables", new {id = item.Id}, item);
        }

        // DELETE tables/User/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteUser(string id)
        {
            return DeleteAsync(id);
        }

        public UserController(IPushNotificationService pushNotificationService) : base(pushNotificationService)
        {
        }
    }
}