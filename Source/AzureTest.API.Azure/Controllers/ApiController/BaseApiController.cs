﻿using System.Web.Http;
using System.Web.Http.Controllers;
using AzureTest.DAL;
using Microsoft.Azure.Mobile.Server.Config;

namespace AzureTest.API.Azure.Controllers.ApiController
{
    [Authorize]
    [MobileAppController]
    public abstract class BaseApiController : System.Web.Http.ApiController
    {
        protected DatabaseContext DbContext { get; private set; }

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            DbContext = new DatabaseContext();
        }
    }
}