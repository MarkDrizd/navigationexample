﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using AzureTest.API.Azure.Services;
using AzureTest.Model;

namespace AzureTest.API.Azure.Controllers.ApiController
{
    public class PushController : BaseApiController
    {
        private readonly IPushNotificationService _pushnotificationService;

        public PushController(IPushNotificationService pushnotificationService)
        {
            _pushnotificationService = pushnotificationService;
        }

        public async Task<IHttpActionResult> Register(string installationId)
        {
            //TODO: Register push notifications for specific user

            try
            {
                await _pushnotificationService.RegisterUserForPush(installationId);
            }
            catch (Exception ex)
            {
                //TODO: Add loger 
            }

            return Ok();
        }

        public async Task<IHttpActionResult> UnRegister(string pushInstallationId)
        {
            try
            {
                await _pushnotificationService.UnRegister(pushInstallationId);
            }
            catch (Exception ex)
            {
                //TODO: Add loger 
            }

            return Ok();
        }

        public async Task<IHttpActionResult> SendTestPush(PushNotification notification, [FromUri] string userId = null)
        {
            if (notification == null)
            {
                notification = new PushNotification { Message = "Test Push Notification", AdditionalData = "Additional Data for Push Test" };
            }

            userId = userId ?? notification.ToId;

            if (userId != null)
            {
                await _pushnotificationService.SendPushNotificationToUser(notification, userId);
            }
            else
            {
                await _pushnotificationService.SendPushNotificationToAll(notification);
            }

            return Ok(notification);
        }
    }
}