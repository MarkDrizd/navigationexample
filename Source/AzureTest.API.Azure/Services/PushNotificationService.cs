﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using AzureTest.Model;
using Microsoft.Azure.NotificationHubs;

namespace AzureTest.API.Azure.Services
{
    public class PushNotificationService : IPushNotificationService
    {
        private static NotificationHubClient _notificationHub;

        public PushNotificationService()
        {
            if (_notificationHub == null)
                _notificationHub =
                    NotificationHubClient.CreateClientFromConnectionString(
                        ConfigurationManager.ConnectionStrings["MS_NotificationHubConnectionString"].ConnectionString,
                        ConfigurationManager.AppSettings["MS_NotificationHubName"]);
        }

        public Task SendPushNotificationToAll(PushNotification notification)
        {
            notification.IsPublic = true;
            return SendPushNotificationToTags(notification, null);
        }

        public Task SendPushNotificationToUser(PushNotification notification, string toUserId)
        {
            notification.ToId = toUserId;
            return SendPushNotificationToTags(notification, $"userId:{toUserId}");
        }

        public async Task SendPushNotificationToTags(PushNotification notification, params string[] tags)
        {
            var templateParams = new Dictionary<string, string>
            {
                ["messageParam"] = notification.Message,
                ["attachmentLinkParam"] = notification.AttachmentLink,
                ["thumbnailLinkParam"] = notification.ThumbnailLink,
                ["additionalDataParam"] = notification.AdditionalData,
                ["toIdParam"] = notification.ToId,
                ["isPublicParam"] = notification.IsPublic.ToString()
            };

            if (tags != null && tags.Any())
            {
                await _notificationHub.SendTemplateNotificationAsync(templateParams, tags);
            }
        }

        public async Task RegisterUserForPush(string installationId, string userId = null)
        {
            var installation = await _notificationHub.GetInstallationAsync(installationId);

            var userTag = $"userId:{userId}";

            if (installation.Tags == null)
                installation.Tags = new List<string>();

            if (!installation.Tags.Contains(userTag))
            {
                installation.Tags.Add(userTag);
                await _notificationHub.CreateOrUpdateInstallationAsync(installation);
            }
        }

        public async Task UnRegister(string installationId)
        {
            await _notificationHub.DeleteInstallationAsync(installationId);
        }
    }
}