﻿using System.Threading.Tasks;
using AzureTest.Model;

namespace AzureTest.API.Azure.Services
{
    public interface IPushNotificationService
    {
        Task SendPushNotificationToAll(PushNotification notification);

        Task SendPushNotificationToUser(PushNotification notification, string userId);

        Task SendPushNotificationToTags(PushNotification notification, params string[] tags);

        Task RegisterUserForPush(string installationId, string userId = null);

        Task UnRegister(string installationId);
    }
}