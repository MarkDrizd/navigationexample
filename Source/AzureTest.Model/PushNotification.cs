﻿namespace AzureTest.Model
{
    public class PushNotification
    {
        public PushNotification()
        {
            
        }

        public PushNotification(string message, string additionalData)
        {
            Message = message;
            AdditionalData = additionalData;
        }

        public string ToId { get; set; }

        public bool IsPublic { get; set; }

        public string Message { get; set; }

        public string AttachmentLink { get; set; }

        public string ThumbnailLink { get; set; }

        public string AdditionalData { get; set; }

        public string Icon { get; set; }

        public bool Read { get; set; }
    }
}
