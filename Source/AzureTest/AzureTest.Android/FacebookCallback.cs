﻿using System;
using Android.Runtime;
using Xamarin.Facebook;

namespace AzureTest.Droid
{
    public class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
    {
        public Action HandleCancel { get; set; }

        public Action<FacebookException> HandleError { get; set; }

        public Action<TResult> HandleSuccess { get; set; }

        public void OnCancel()
        {
            var c = HandleCancel;
            c?.Invoke();
        }

        public void OnError(FacebookException error)
        {
            var c = HandleError;
            c?.Invoke(error);
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            var c = HandleSuccess;
            c?.Invoke(result.JavaCast<TResult>());
        }
    }
}