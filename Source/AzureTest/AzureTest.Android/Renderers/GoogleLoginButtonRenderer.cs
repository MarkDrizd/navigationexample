﻿using System;
using Android.Gms.Auth.Api;
using AzureTest.Controls;
using AzureTest.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(GoogleLoginButton), typeof(GoogleLoginButtonRenderer))]
namespace AzureTest.Droid.Renderers
{
    public class GoogleLoginButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var mainActivity = Context as MainActivity;

                if (Control != null)
                {
                    Control.Click += (sender, ee) =>
                    {
                        var signInIntent = Auth.GoogleSignInApi.GetSignInIntent(mainActivity?.GoogleApiClient);
                        ((FormsAppCompatActivity) Forms.Context).StartActivityForResult(signInIntent, MainActivity.GoogleSignInCode);
                    };
                }

                if (mainActivity != null)
                {
                    mainActivity.SetGoogleLoginResult = SetLoginResult;
                    mainActivity.SetLoginError = SetLoginError;
                }
            }
        }

        private void SetLoginResult(string accessToken)
        {
            var googleButton = Element as GoogleLoginButton;

            if (googleButton == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(accessToken))
            {
                if (googleButton.AfterLoginCommand != null)
                {
                    if (googleButton.AfterLoginCommand.CanExecute(accessToken))
                    {
                        googleButton.AfterLoginCommand.Execute(accessToken);
                    }
                }
            }
            else
            {
                googleButton.HandleLoginErrorCommand?.Execute(new Exception("AccessToken is empty"));
            }
        }

        private void SetLoginError(Exception ex)
        {
            var googleButton = Element as GoogleLoginButton;

            googleButton?.HandleLoginErrorCommand?.Execute(ex);
        }
    }
}

 
