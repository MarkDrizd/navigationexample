﻿using System;
using AzureTest.Commands;
using AzureTest.Controls;
using AzureTest.Droid.Renderers;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(FacebookLoginButton), typeof(FacebookLoginButtonRenderer))]
namespace AzureTest.Droid.Renderers
{
    public class FacebookLoginButtonRenderer : ButtonRenderer
    {
        private LoginButton _loginButton;

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            var fbButton = Element as FacebookLoginButton;

            if (_loginButton == null)
            {
                _loginButton = new LoginButton(Context);
                _loginButton.SetReadPermissions("user_birthday", "public_profile", "email", "user_friends", "user_location"); // Todo: share those permissions with all apps, perhaps in Res ?
                _loginButton.LoginBehavior = LoginBehavior.NativeWithFallback;
            }

            if (fbButton != null && _loginButton != null && fbButton.InvokeLoginCommand == null)
            {
                fbButton.InvokeLoginCommand = new RelayCommand(() => _loginButton.PerformClick());
            }

            if (!(Control is LoginButton))
            {
                SetNativeControl(_loginButton);
            }

            var mainActivity = Context as MainActivity;

            if (mainActivity != null)
            {
                mainActivity.SetFacebbokLoginResult = SetLoginResult;
                mainActivity.SetLoginError = SetLoginError;
            }
        }

        private void SetLoginResult(string accessToken)
        {
            var fbButton = Element as FacebookLoginButton;

            if (fbButton == null)
                return;

            if (!string.IsNullOrEmpty(accessToken))
            {
                if (fbButton.AfterLoginCommand != null)
                {
                    if (fbButton.AfterLoginCommand.CanExecute(accessToken))
                    {
                        fbButton.AfterLoginCommand.Execute(accessToken);
                    }
                }
            }
            else
            {
                fbButton.HandleLoginErrorCommand?.Execute(new Exception("AccessToken is empty"));
            }
        }

        private void SetLoginError(Exception ex)
        {
            var fbButton = Element as FacebookLoginButton;

            fbButton?.HandleLoginErrorCommand?.Execute(ex);
        }
    }
}