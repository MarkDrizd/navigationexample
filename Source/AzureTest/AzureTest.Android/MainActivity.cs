﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Auth.Api;
using Android.Gms.Auth.Api.SignIn;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Autofac;
using AzureTest.Droid.Services;
using AzureTest.Model;
using AzureTest.Services;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Forms;
using Debug = System.Diagnostics.Debug;

//Initialize FacebookActivity
[assembly: MetaData("com.facebook.sdk.ApplicationId", Value = "@string/fb_app_id")]
namespace AzureTest.Droid
{
    [Activity(Label = "AzureTest", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity, GoogleApiClient.IOnConnectionFailedListener, GoogleApiClient.IConnectionCallbacks
    {
        public const int GoogleSignInCode = 9001;

        public Action<string> SetGoogleLoginResult;
        public Action<string> SetFacebbokLoginResult;
        public Action<Exception> SetLoginError;

        public GoogleApiClient GoogleApiClient;

        public static MainActivity Instance { get; private set; }
        public bool IsVisible { get; set; }

        private ICallbackManager _fbCallbackManager;
      
        protected override void OnCreate(Bundle bundle)
        {
            Instance = this;

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            InitFacebook();
            ConfigureGoogleSignIn();

            Forms.Init(this, bundle);
            LoadApplication(new App());

            CurrentPlatform.Init();

            InitPush();
        }

        private void InitPush()
        {
            if (!IsGooglePlayServicesInstalled())
            {
                return;
            }

            var intent = new Intent(this, typeof(PushRegistrationIntentService));
            StartService(intent);
        }

        private bool IsGooglePlayServicesInstalled()
        {
            var queryResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            return queryResult == ConnectionResult.Success;
        }

        private PushNotification GetPushNotification(Intent intent)
        {
            if (intent?.Extras == null)
            {
                return null;
            }

            try
            {
                var notificationString = intent.Extras.GetString("notification");

                if (notificationString != null)
                {
                    return JsonConvert.DeserializeObject<PushNotification>(notificationString);
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetPushNotification", ex.Message);
            }

            return null;
        }

        private void InitFacebook()
        {
            _fbCallbackManager = CallbackManagerFactory.Create();

            var loginCallback = new FacebookCallback<LoginResult>
            {
                HandleSuccess = loginResult => { SetFacebbokLoginResult?.Invoke(loginResult.AccessToken.Token); },
                HandleError = ex => { SetLoginError?.Invoke(ex); }
            };

            LoginManager.Instance.RegisterCallback(_fbCallbackManager, loginCallback);
        }

        private void ConfigureGoogleSignIn()
        {
            var googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
                .RequestEmail()
                .RequestIdToken("1053052751937-vddnq2hqotfvrqq6vis016g9a2sgfr3g.apps.googleusercontent.com") //TODO: Get key from google-service.json
                .Build();

            GoogleApiClient = new GoogleApiClient.Builder(this)
                .EnableAutoManage(this, this)
                .AddApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .Build();
        }

        private void HandleGoogleSignInResult(GoogleSignInResult result)
        {
            if (result.IsSuccess)
            {
                var googleAccountInfo = result.SignInAccount;
                SetGoogleLoginResult?.Invoke(googleAccountInfo.IdToken);
            }
            else
            {
                SetLoginError?.Invoke(new Exception("Can't connect to google profile"));
            }
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            _fbCallbackManager?.OnActivityResult(requestCode, (int) resultCode, data);

            if (requestCode == GoogleSignInCode)
            {
                var result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);
                HandleGoogleSignInResult(result);
            }

            if (requestCode == 32) // Push Notification Tap
            {
                var notification = GetPushNotification(data);
                var app = Xamarin.Forms.Application.Current as App;

                if (notification != null)
                {
                    var pushService = app?.Container.Resolve<IPushNotificationService>();
                    pushService?.NavigateToDetails(notification);
                }
            }
        }

        #region IConnectionCallbacks implementation

        public void OnConnected(Bundle connectionHint)
        {
            Debug.WriteLine("Connected" + connectionHint);
        }

        public void OnConnectionSuspended(int cause)
        {
            Debug.WriteLine("Connection suspended" + cause);
        }

        public void OnConnectionFailed(ConnectionResult result)
        {
            Debug.WriteLine(result.ErrorMessage, result.ErrorCode);
        }

        #endregion

        #region App Life Cycle

        protected override void OnPause()
        {
            IsVisible = false;
            Instance = this;
            base.OnPause();

        }

        protected override void OnStop()
        {
            IsVisible = false;
            Instance = this;
            base.OnStop();
        }

        protected override void OnStart()
        {
            IsVisible = true;
            Instance = this;
            base.OnStart();
        }

        protected override void OnResume()
        {
            IsVisible = true;
            Instance = this;
            base.OnResume();
        }

        #endregion
    }
}

