﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Android.App;
using Android.Content;
using Android.Gms.Gcm;
using Android.Gms.Iid;
using Android.Util;
using Autofac;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AzureTest.Droid.Services
{
    [Service(Exported = false)]
    public class PushRegistrationIntentService : IntentService
    {
        private static readonly object Locker = new object();
        private App App => Xamarin.Forms.Application.Current as App;

        public static string RegistrationId { get; private set; }

        protected override void OnHandleIntent(Intent intent)
        {
            try
            {
                Log.Info("PushRegistrationIntentService", "Calling InstanceID.GetToken");

                lock (Locker)
                {
                    var instanceId = InstanceID.GetInstance(this);
                    var token = instanceId.GetToken("1053052751937", GoogleCloudMessaging.InstanceIdScope, null);

                    Log.Info("PushRegistrationIntentService", "GCM Registration Token: " + token);

                    Subscribe(token);
                    RegistrationId = token;

                    var push = App.Container.Resolve<IMobileServiceClient>().GetPush();
                    MainActivity.Instance.RunOnUiThread(() => Register(push, null));
                }
            }
            catch (Exception ex)
            {
                Log.Debug("PushRegistrationIntentService", ex.Message);
            }
        }

        public async void Register(Push push, IEnumerable<string> tags)
        {
            try
            {
                if (App == null || !string.IsNullOrWhiteSpace(App.PushInstallationId))
                {
                    return;
                }

                //TODO: Add notification tag to backend

                var templateBodyGcm = JsonConvert.SerializeObject(
                    new
                    {
                        data =
                        new
                        {
                            message = "$(messageParam)",
                            attachmentLink = "$(attachmentLinkParam)",
                            thumbnailLink = "$(thumbnailLinkParam)",
                            additionalData = "$(additionalDataParam)",
                            toId = "$(toIdParam)",
                            isPublic = "$(isPublicParam)"
                        }
                    });

                var templates = new JObject
                {
                    ["genericMessage"] = new JObject
                    {
                        {"body", templateBodyGcm}
                    }
                };

                await push.RegisterAsync(RegistrationId, templates);
                Log.Info("Push Installation Id", push.InstallationId);
                App.PushInstallationId = push.InstallationId;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void Subscribe(string token)
        {
            var pubSub = GcmPubSub.GetInstance(this);
            pubSub.Subscribe(token, "/topics/global", null);
        }
    }
}