﻿using Android.App;
using Android.Content;
using Android.Gms.Iid;

namespace AzureTest.Droid.Services
{
    [Service(Exported = false), IntentFilter(new[] { "com.google.android.gms.iid.InstanceID" })]
    public class PushInstanceIdListenerService : InstanceIDListenerService
    {
        public override void OnTokenRefresh()
        {
            var intent = new Intent(this, typeof(PushRegistrationIntentService));
            StartService(intent);
        }
    }
}