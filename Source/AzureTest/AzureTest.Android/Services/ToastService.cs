﻿using System;
using Android.Support.Design.Widget;
using Android.Views;
using AzureTest.Droid.Services;
using AzureTest.Services;

[assembly: Xamarin.Forms.Dependency(typeof(ToastService))]
namespace AzureTest.Droid.Services
{
    public class ToastService : IToastService
    {
        public void ShowToast(string message, string title = null, string icon = null, int duration = 5,
            Action onTap = null, Action onDismiss = null)
        {
            if (duration < 1)
            {
                duration = 1;
            }

            var mainView = MainActivity.Instance.Window.DecorView.FindViewById(Android.Resource.Id.Content) as ViewGroup;

            if (mainView == null || mainView.ChildCount == 0)
            {
                return;
            }

            var snackBar = Snackbar.Make(mainView.GetChildAt(0), message, duration * 1000);

            if (onTap != null)
            {
                //TODO: Move string to resources 
                snackBar.SetAction("ckech it", view => { onTap.Invoke(); });
            }

            snackBar.Show();
        }
    }
}