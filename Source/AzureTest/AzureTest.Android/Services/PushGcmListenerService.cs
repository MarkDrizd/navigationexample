﻿using Android.App;
using Android.Content;
using Android.Gms.Gcm;
using Android.OS;
using Android.Support.V4.App;
using Autofac;
using AzureTest.Model;
using AzureTest.Services;
using Newtonsoft.Json;

namespace AzureTest.Droid.Services
{
    [Service(Exported = false), IntentFilter(new[] { "com.google.android.c2dm.intent.RECEIVE" })]
    public class PushGcmListenerService : GcmListenerService
    {
        private IPushNotificationService _pushNotificationService;
        private App App => Xamarin.Forms.Application.Current as App;

        public override void OnMessageReceived(string from, Bundle data)
        {
            if (data != null)
            {
                var message = data.GetString("message");

                if (string.IsNullOrEmpty(message))
                {
                    message = data.GetString("msg");
                }

                var attachmentLink = data.GetString("attachmentLink");

                var thumbnailLink = data.GetString("thumbnailLink");

                var additionalData = data.GetString("additionalData");

                var toId = data.GetString("toId");

                bool isPublic;
                bool.TryParse(data.GetString("isPublic"), out isPublic);

                //TODO: Display notification to autorized users only

                var notification = new PushNotification(message, additionalData)
                {
                    AttachmentLink = attachmentLink,
                    ThumbnailLink = thumbnailLink
                };

                if (MainActivity.Instance != null && MainActivity.Instance.IsVisible)
                {
                    _pushNotificationService = App.Container.Resolve<IPushNotificationService>();

                    if (_pushNotificationService.ShouldCreateLocalNotification(notification))
                    {
                        _pushNotificationService.CreateLocalNotification(notification);
                        _pushNotificationService.HandlePushNotification(notification);
                    }
                }
                else
                {
                    CreateNotification(notification);
                }

            }
        }

        private void CreateNotification(PushNotification notification)
        {
            //Create notification
            var notificationManager = GetSystemService(NotificationService) as NotificationManager;

            //Create an intent to show ui
            var uiIntent = new Intent(this, typeof(MainActivity));
            uiIntent.PutExtra("notification", JsonConvert.SerializeObject(notification));

            //Set the notification info
            //we use the pending intent, passing our ui intent over which will get called
            //when the notification is tapped.

            var builder = new NotificationCompat.Builder(this);

            var localNotification = builder.SetContentIntent(PendingIntent.GetActivity(this, 32, uiIntent, PendingIntentFlags.UpdateCurrent))
                .SetSmallIcon(Resource.Drawable.icon).SetTicker(notification.Message)
                .SetAutoCancel(true).SetContentTitle("AzureTest")
                .SetDefaults((int)(NotificationDefaults.Sound | NotificationDefaults.Vibrate))
                .SetContentText(notification.Message).Build();

            notificationManager?.Notify(1, localNotification);
        }
    }
}