﻿using System;
using System.Diagnostics;
using AzureTest.Commands;
using AzureTest.Controls;
using AzureTest.iOS.Renderers;
using Facebook.LoginKit;
using Foundation;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(FacebookLoginButton), typeof(FacebookLoginButtonRenderer))]
namespace AzureTest.iOS.Renderers
{
    public class FacebookLoginButtonRenderer : ButtonRenderer
    {
        private LoginButton _loginButton;

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            var fbButton = Element as FacebookLoginButton;

            if (_loginButton == null)
            {
                _loginButton = new LoginButton
                {
                    ReadPermissions =
                        new[]
                        {
                            "user_birthday", "public_profile", "email", "user_friends", "user_location"
                        } // Todo: share those permissions with all apps, perhaps in Res ?
                };

                _loginButton.Completed += LoginButton_Completed;
            }

            try
            {
                SetNativeControl(_loginButton);
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
            }

            _loginButton.LoginBehavior = LoginBehavior.Native;

            if (fbButton != null)
            {
                fbButton.InvokeLoginCommand = new RelayCommand(() => _loginButton.SendActionForControlEvents(UIKit.UIControlEvent.TouchUpInside));
            }
        }

        private void LoginButton_Completed(object sender, LoginButtonCompletedEventArgs e)
        {
            var fbButton = Element as FacebookLoginButton;

            if (fbButton?.AfterLoginCommand == null)
            {
                return;
            }

            if (e.Error != null)
            {
                fbButton.HandleLoginErrorCommand?.Execute(new NSErrorException(e.Error));
                return;
            }

            var accessToken = e.Result?.Token?.TokenString;

            if (!string.IsNullOrEmpty(accessToken) && fbButton.AfterLoginCommand.CanExecute(accessToken))
            {
                fbButton.AfterLoginCommand.Execute(accessToken);
            }
        }
    }
}