﻿using System;
using AzureTest.Controls;
using AzureTest.iOS.Renderers;
using Foundation;
using Google.SignIn;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(GoogleLoginButton), typeof(GoogleLoginButtonRenderer))]
namespace AzureTest.iOS.Renderers
{
    public class GoogleLoginButtonRenderer : ButtonRenderer, ISignInDelegate, ISignInUIDelegate
    {
        private SignInButton _loginButton;

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (_loginButton == null)
            {
                _loginButton = new SignInButton();
            }
          
            SignIn.SharedInstance.UIDelegate = new GoogleSignInUiDelegate();
            SignIn.SharedInstance.Delegate = this;

            if (Control != null)
            {
                Control.TouchUpInside += (sender, ee) =>
                {
                    SignIn.SharedInstance.SignInUser();
                };
            }
        }

        public void DidSignIn(SignIn signIn, GoogleUser user, NSError error)
        {
            var googleButton = Element as GoogleLoginButton;

            if (googleButton == null)
            {
                return;
            }

            if (error != null)
            {
                googleButton.HandleLoginErrorCommand?.Execute(new Exception(error.LocalizedDescription));
            }

            var accessToken = signIn.CurrentUser.Authentication.IdToken;

            if (!string.IsNullOrEmpty(accessToken))
            {
                if (googleButton.AfterLoginCommand != null)
                {
                    if (googleButton.AfterLoginCommand.CanExecute(accessToken))
                    {
                        googleButton.AfterLoginCommand.Execute(accessToken);
                    }
                }
            }
            else
            {
                googleButton.HandleLoginErrorCommand?.Execute(new Exception("AccessToken is empty"));
            }
        }
    }
}