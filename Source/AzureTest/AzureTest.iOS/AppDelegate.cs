﻿using System;
using Autofac;
using AzureTest.Extensions;
using AzureTest.Model;
using AzureTest.Services;
using Facebook.CoreKit;
using Foundation;
using Google.SignIn;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UIKit;
using Xamarin.Forms;

namespace AzureTest.iOS
{   //TODO: Implement new iOS notifications
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        private const string FacebookAppId = "451045621932501";
        private const string FacebookAppName = "AzureTest";

        private App App => Xamarin.Forms.Application.Current as App;
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            CurrentPlatform.Init();
            
            Forms.Init();
            LoadApplication(new App());

            var googleServiceDictionary = NSDictionary.FromFile("GoogleService-Info.plist");

            SignIn.SharedInstance.ClientID = googleServiceDictionary["CLIENT_ID"].ToString();
            SignIn.SharedInstance.ServerClientID = "1053052751937-vddnq2hqotfvrqq6vis016g9a2sgfr3g.apps.googleusercontent.com"; //TODO: Move to a constants class 

            Settings.AppID = FacebookAppId;
            Settings.DisplayName = FacebookAppName;

            RegisterPushNotifications();

            if (options != null && options.ContainsKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")))
            {
                var pushDict = options.ObjectForKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")) as NSDictionary;

                GetPushNotification(pushDict);
            }

            return base.FinishedLaunching(app, options);
        }

        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            var openUrlOptions = new UIApplicationOpenUrlOptions(options);
            return SignIn.SharedInstance.HandleUrl(url, openUrlOptions.SourceApplication, openUrlOptions.Annotation);
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            //TODO: Configure tags in backend
            var templateBodyApns = JsonConvert.SerializeObject(
                new
                {
                    aps = new { alert = "$(messageParam)", badge = 1 },
                    attachmentLink = "$(attachmentLinkParam)",
                    thumbnailLink = "$(thumbnailLinkParam)",
                    type = "$(typeParam)",
                    additionalData = "$(additionalDataParam)",
                    toId = "$(toIdParam)",
                    isPublic = "$(isPublicParam)"
                });

            JObject templates = new JObject
            {
                ["genericMessage"] = new JObject
                {
                    {"body", templateBodyApns}
                }
            };

            var app = Xamarin.Forms.Application.Current as App;
            var push = app?.Container.Resolve<IMobileServiceClient>().GetPush();

            push?.RegisterAsync(deviceToken, templates)
                .ContinueWith(t => { App.PushInstallationId = push.InstallationId; })
                .RunAsynchronously();
        }

        public override async void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo,
            Action<UIBackgroundFetchResult> completionHandler)
        {
            var notification = GetPushNotification(userInfo);
            var notificationService = App.Container.Resolve<IPushNotificationService>();

            //TODO: Display notifications for autorized users only
            if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active)
            {
                if (notificationService.ShouldCreateLocalNotification(notification))
                {
                    notificationService.CreateLocalNotification(notification);
                    notificationService.HandlePushNotification(notification);
                }
            }
            else
            {
                await notificationService.NavigateToDetails(notification);
            }
        }

        private void RegisterPushNotifications()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                {
                    var settings = UIUserNotificationSettings.GetSettingsForTypes(
                        UIUserNotificationType.Alert |
                        UIUserNotificationType.Badge |
                        UIUserNotificationType.Sound,
                        new NSSet());


                    UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
                    UIApplication.SharedApplication.RegisterForRemoteNotifications();
                }
                else
                {
                    UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(
                        UIRemoteNotificationType.Alert |
                        UIRemoteNotificationType.Badge |
                        UIRemoteNotificationType.Sound);
                }
            });
        }

        private PushNotification GetPushNotification(NSDictionary userInfo, bool handlePush = false)
        {
            var aps = userInfo.ObjectForKey(new NSString("aps")) as NSDictionary;

            var message = string.Empty;
            var attachmentLink = string.Empty;
            var thumbnailLink = string.Empty;
            var type = string.Empty;
            var additionalData = string.Empty;
            var toId = string.Empty;
            bool isPublic;

            if (aps != null && aps.ContainsKey(new NSString("alert")))
            {
                message = (aps[new NSString("alert")] as NSString)?.ToString();
            }

            if (userInfo.ContainsKey(new NSString("attachmentLink")))
            {
                attachmentLink = (userInfo[new NSString("attachmentLink")] as NSString)?.ToString();
            }

            if (userInfo.ContainsKey(new NSString("thumbnailLink")))
            {
                thumbnailLink = (userInfo[new NSString("thumbnailLink")] as NSString)?.ToString();
            }

            if (userInfo.ContainsKey(new NSString("type")))
            {
                type = (userInfo[new NSString("type")] as NSString)?.ToString();
            }

            if (userInfo.ContainsKey(new NSString("additionalData")))
            {
                additionalData = (userInfo[new NSString("additionalData")] as NSString)?.ToString();
            }

            if (userInfo.ContainsKey(new NSString("toId")))
            {
                toId = (userInfo[new NSString("toId")] as NSString)?.ToString();
            }

            if (userInfo.ContainsKey(new NSString("isPublic")))
            {
                bool.TryParse((userInfo[new NSString("isPublic")] as NSString)?.ToString(), out isPublic);
            }
            else
            {
                isPublic = false;
            }

            var pushNotification = new PushNotification(message, additionalData)
            {
                AttachmentLink = attachmentLink,
                ThumbnailLink = thumbnailLink
            };

            return pushNotification;
        }
    }
}
