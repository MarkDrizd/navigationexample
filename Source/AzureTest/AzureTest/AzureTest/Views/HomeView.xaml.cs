﻿using AzureTest.Views.Base;
using Xamarin.Forms.Xaml;

namespace AzureTest.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomeView : BasePage
	{
		public HomeView()
		{
			InitializeComponent ();
		}
	}
}