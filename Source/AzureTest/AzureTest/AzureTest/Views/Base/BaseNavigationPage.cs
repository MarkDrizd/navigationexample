﻿using Xamarin.Forms;

namespace AzureTest.Views.Base
{
    public class BaseNavigationPage : NavigationPage
    {
        public BaseNavigationPage(Page root, bool presentToolbar = true) : base(root)
        {
            if (!presentToolbar)
            {
                SetHasNavigationBar(root, false);
            }
        }

        protected override void OnAppearing()
        {
            Popped += Page_Popped;
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            Popped -= Page_Popped;
            base.OnDisappearing();
        }

        private void Page_Popped(object sender, NavigationEventArgs e)
        {
            var basePage = e.Page as IBasePage;
            basePage?.OnPageClosing();
        }
    }
}
