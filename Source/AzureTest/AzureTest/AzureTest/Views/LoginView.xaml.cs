﻿using AzureTest.Views.Base;
using Xamarin.Forms.Xaml;

namespace AzureTest.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginView : BasePage
	{
		public LoginView ()
		{
			InitializeComponent ();
		}
	}
}