﻿using System.Threading.Tasks;
using AzureTest.Model;

namespace AzureTest.PushHandlers
{
    public abstract class BaseNotificationHandler : IPushNotificationHandler
    {
        public abstract void Handle(PushNotification notification);

        public abstract Task NavigateToDetails(PushNotification notification);
    }
}
