﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using AzureTest.Model;
using AzureTest.Models;
using AzureTest.Services;
using AzureTest.ViewModels;
using Newtonsoft.Json;

namespace AzureTest.PushHandlers
{
    public class BlogNotificationHandler : BaseNotificationHandler
    {
        private readonly INavigationService _navigationService;

        public BlogNotificationHandler(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override void Handle(PushNotification notification)
        {
            //TODO add handle logic
        }

        public override async Task NavigateToDetails(PushNotification notification)
        {
            if (!string.IsNullOrWhiteSpace(notification?.AdditionalData))
            {
                try
                {
                    var group = JsonConvert.DeserializeObject<Blog>(notification.AdditionalData);
                    if (group != null)
                    {   //TODO: navigate to apropriate ViewModel
                        await _navigationService.NavigateToAsync<HomeViewModel>(group);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }
    }
}
