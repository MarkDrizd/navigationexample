﻿using System.Threading.Tasks;
using AzureTest.Model;

namespace AzureTest.PushHandlers
{
    public interface IPushNotificationHandler
    {
        void Handle(PushNotification notification);

        Task NavigateToDetails(PushNotification notification);
    }
}
