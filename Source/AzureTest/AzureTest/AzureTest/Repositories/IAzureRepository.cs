﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AzureTest.Models;
using Microsoft.WindowsAzure.MobileServices;

namespace AzureTest.Repositories
{
    public interface IAzureRepository<T> where T : BaseModel 
    {
        Task InsertAsync(T instance, bool pushToRemote = true);

        Task DeleteAsync(T instace, bool pushToRemote = true);

        Task UpdateAsync(T instance, bool pushToRemote = true);

        Task<T> LookupAsync(string id, bool pullBefore = false, IDictionary<string, string> queryParameters = null);

        Task RefreshAsync(T instance, bool pullBefore = false);

        Task<MobileServiceCollection<T>> GetRemoteItemsOnlyAsync(Expression<Func<T, bool>> predicate, bool latestFirst = false);

        Task<long> Count(Expression<Func<T, bool>> predicate = null);

        Task UpsertAsync(T entity, bool pushToRemote = true);

        Task AddToCache(params T[] entities);

        Task<MobileServiceCollection<T>> GetItemsAsync(Expression<Func<T, bool>> predicate,
            bool pullBefore = false, string queryName = null, bool latestFirst = false,
            IDictionary<string, string> queryParameters = null);
    }
}
