﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AzureTest.Models;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.Sync;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AzureTest.Repositories
{
    public class AzureRepository<T> : IAzureRepository<T> where T : BaseModel
    {
        protected readonly CancellationTokenSource CancellationTokenSource;
        protected readonly IMobileServiceSyncTable<T> LocalTable;
        protected readonly IMobileServiceClient MobileClient;

        protected virtual int PageSize => 25;

        //TODO: Add connectivity plugin and check connection
        protected virtual bool OfflineMode => false;

        public AzureRepository(IMobileServiceClient mobileClient)
        {
            MobileClient = mobileClient;
            LocalTable = mobileClient.GetSyncTable<T>();

            CancellationTokenSource = new CancellationTokenSource();
        }

        public async Task InsertAsync(T entity, bool pushToRemote = true)
        {
            if (entity.Id == null)
            {
                entity.Id = Guid.NewGuid().ToString("N");
            }

            await LocalTable.InsertAsync(entity);

            if (pushToRemote && !OfflineMode)
            {
                await MobileClient.SyncContext.PushAsync();
            }
        }

        public async Task DeleteAsync(T entity, bool pushToRemote = true)
        {
            await LocalTable.DeleteAsync(entity);

            if (pushToRemote && !OfflineMode)
            {
                await MobileClient.SyncContext.PushAsync();
            }
        }

        public async Task UpdateAsync(T entity, bool pushToRemote = true)
        {
            await LocalTable.UpdateAsync(entity);

            if (pushToRemote && !OfflineMode)
            {
                await MobileClient.SyncContext.PushAsync();
            }
        }

        public async Task<T> LookupAsync(string id, bool pullBefore = false,
            IDictionary<string, string> queryParameters = null)
        {
            if (pullBefore && !OfflineMode)
            {
                return (await GetItemsAsync(x => x.Id == id, true, null, false, queryParameters)).FirstOrDefault();
            }

            return await LocalTable.LookupAsync(id);
        }

        public async Task RefreshAsync(T entity, bool pullBefore = false)
        {
            if (pullBefore && !OfflineMode)
            {
                entity = (await GetItemsAsync(x => x.Id == entity.Id, true)).FirstOrDefault();
            }

            await LocalTable.RefreshAsync(entity);
        }

        public async Task<MobileServiceCollection<T>> GetRemoteItemsOnlyAsync(Expression<Func<T, bool>> predicate,
            bool latestFirst = false)
        {
            if (OfflineMode)
            {
                return null;
            }

            var azureQuery = MobileClient.GetTable<T>().CreateQuery().Where(predicate);

            if (latestFirst)
            {
                azureQuery = azureQuery.OrderByDescending(x => x.CreatedAt);
            }

            var items = new MobileServiceCollection<T>(azureQuery, PageSize);

            await items.LoadMoreItemsAsync();

            return items;
        }

        public async Task<long> Count(Expression<Func<T, bool>> predicate = null)
        {
            var query = predicate == null
                ? LocalTable.CreateQuery().Take(0).IncludeTotalCount()
                : LocalTable.CreateQuery().Where(predicate).Take(0).IncludeTotalCount();

            var items = new MobileServiceCollection<T>(query);

            await items.LoadMoreItemsAsync();
            return items.TotalCount;
        }

        public Task UpsertAsync(T entity, bool pushToRemote = true)
        {
            return string.IsNullOrEmpty(entity.Id)
                ? InsertAsync(entity, pushToRemote)
                : UpdateAsync(entity, pushToRemote);
        }

        public Task AddToCache(params T[] entities)
        {
            return MobileClient.SyncContext.Store.UpsertAsync(typeof(T).Name,
                entities.Select(x => JObject.FromObject(x,
                        new JsonSerializer {ReferenceLoopHandling = ReferenceLoopHandling.Ignore}))
                    .ToList(), true);
        }

        public async Task<MobileServiceCollection<T>> GetItemsAsync(Expression<Func<T, bool>> predicate,
            bool pullBefore = false, string queryName = null, bool latestFirst = false,
            IDictionary<string, string> queryParameters = null)
        {
            var azureQuery = predicate != null ? LocalTable.CreateQuery().Where(predicate) : LocalTable.CreateQuery();

            if (latestFirst && queryName == null)
            {
                azureQuery = azureQuery.OrderByDescending(x => x.CreatedAt);
            }

            if (queryParameters != null)
            {
                azureQuery = azureQuery.WithParameters(queryParameters);
            }

            if (pullBefore && !OfflineMode)
            {
                try
                {
                    if (queryName != null && queryName.Length > 25) // Just in case...
                    {
                        Debug.WriteLine(
                            $"Warning! QueryName {queryName} exceeds 25 chars length. It will be truncated to 25 chars.");
                        queryName = queryName.Substring(0, 25);
                    }

                    await LocalTable.PullAsync(queryName, azureQuery);
                }
                catch (TaskCanceledException ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                catch (WebException ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }

            var items = new MobileServiceCollection<T>(azureQuery, PageSize);

            await items.LoadMoreItemsAsync();

            return items;
        }
    }
}
