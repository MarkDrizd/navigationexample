﻿using System.Windows.Input;
using Xamarin.Forms;

namespace AzureTest.Controls
{
    public class SocialLoginButton : RoundedButton
    {
        public static readonly BindableProperty AfterLoginCommandProperty = BindableProperty.Create("AfterLoginCommand", typeof(ICommand), typeof(SocialLoginButton), default(ICommand));

        public static readonly BindableProperty HandleLoginErrorCommandProperty = BindableProperty.Create("HandleLoginErrorCommand", typeof(ICommand), typeof(SocialLoginButton), default(ICommand));

        public static readonly BindableProperty InvokeLoginCommandProperty = BindableProperty.Create("InvokeLoginCommand", typeof(ICommand), typeof(SocialLoginButton), default(ICommand), BindingMode.OneWayToSource);
        
        public ICommand AfterLoginCommand
        {
            get => GetValue(AfterLoginCommandProperty) as ICommand;
            set => SetValue(AfterLoginCommandProperty, value);
        }

        public ICommand HandleLoginErrorCommand
        {
            get => GetValue(HandleLoginErrorCommandProperty) as ICommand;
            set => SetValue(HandleLoginErrorCommandProperty, value);
        }

        public ICommand InvokeLoginCommand
        {
            get => GetValue(InvokeLoginCommandProperty) as ICommand;
            set => SetValue(InvokeLoginCommandProperty, value);
        }

        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == IsEnabledProperty.PropertyName)
            {
                Opacity = IsEnabled ? 1 : 0.3;
            }
        }
    }
}
