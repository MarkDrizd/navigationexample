﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace AzureTest.Extensions
{
    public static class TaskExtensions
    {
        public static async void RunAsynchronously(this Task task)
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }
    }
}
