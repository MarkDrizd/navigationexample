﻿using Autofac;
using AzureTest.Extensions;
using AzureTest.Services;
using AzureTest.ViewModels;
using System.Reflection;
using System.Threading.Tasks;
using AzureTest.Repositories;
using Xamarin.Forms.Xaml;

namespace AzureTest
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class App
    {
        public IContainer Container { get; private set; }
        public static string PushInstallationId { get; set; }

        public App()
        {
            InitializeComponent();
           
            SetupDependencyInjection();
            SetStartPage();
        }

        private void SetupDependencyInjection()
        {
            if (Container != null)
            {
                return;
            }              

            var builder = new ContainerBuilder();
            builder.RegisterMvvmComponents(typeof(App).GetTypeInfo().Assembly);
            builder.RegisterXamDependency<IToastService>();
            builder.RegisterType<NavigationService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<PushNotificationService>().AsImplementedInterfaces().SingleInstance();
            builder.Register(c => new MobileClientService(Identifiers.ApiUrl)).AsImplementedInterfaces().SingleInstance();
            builder.RegisterGeneric(typeof(AzureRepository<>)).As(typeof(IAzureRepository<>));
            builder.RegisterPushHandlers(typeof(App).GetTypeInfo().Assembly);

            Container = builder.Build();
        }

        public void SetStartPage()
        {
            var mobileService = Container.Resolve<IMobileClientService>();
          
            //TODO: move database name to constants class
            Task.Run(async () => { await mobileService.InitLocalDatabaseAsync("newdb"); }).Wait();

            var navigationService = Container.Resolve<INavigationService>();
            navigationService.SetMainViewModel<LoginViewModel>();
        }

        protected override void OnStart()
        {
            // Handle when your app start
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}