﻿namespace AzureTest.Models
{
    public class Blog : BaseModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
