﻿using AzureTest.Converters;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using System;

namespace AzureTest.Models
{
    public class BaseModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }      

        [CreatedAt]
        [JsonProperty(PropertyName = "createdAt")]
        public DateTimeOffset? CreatedAt { get; set; }

        [UpdatedAt]
        [JsonProperty(PropertyName = "updatedAt")]
        public DateTimeOffset? UpdatedAt { get; set; }

        [Deleted]
        [JsonConverter(typeof(BoolJsonConverter))]
        [JsonProperty(PropertyName = "deleted")]
        public bool Deleted { get; set; }
    }
}
