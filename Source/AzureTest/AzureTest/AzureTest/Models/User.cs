﻿namespace AzureTest.Models
{
    public class User : BaseModel
    {
        public string ExternalProviderUserId { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }
    }
}
