﻿using AzureTest.ViewModels.Base;
using System;
using Xamarin.Forms;

namespace AzureTest.PageLocators
{
    public interface IPageLocator
    {
        Page ResolvePageAndViewModel(Type viewModelType, object args = null);

        Page ResolvePage(IBaseViewModel viewModel);

        Type ResolvePageType(Type viewmodel);
    }
}
