﻿using Autofac;
using AzureTest.ViewModels.Base;
using AzureTest.Views.Base;
using System;

namespace AzureTest.PageLocators
{
    public class AutofacPageLocator : PageLocator
    {
        private readonly ILifetimeScope _container;

        public AutofacPageLocator(ILifetimeScope container)
        {
            _container = container;
        }

        protected override IBasePage CreatePage(Type pageType)
        {
            return _container.Resolve(pageType) as IBasePage;
        }

        protected override IBaseViewModel CreateViewModel(Type viewModelType)
        {
            return _container.Resolve(viewModelType) as IBaseViewModel;
        }
    }
}
