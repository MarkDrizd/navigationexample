﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace AzureTest.Commands
{
    public class RelayCommandWithArgsAsync<T> : BaseCommand
    {
        public RelayCommandWithArgsAsync(Func<T, Task> action, string message = null)
            : base(message)
        {
            _actionExecute = action;
        }

        public override async void Execute(object parameter)
        {
            try
            {
                await _actionExecute.Invoke((T) parameter);
                SuccessCommand = true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                SuccessCommand = false;
            }
        }

        private readonly Func<T, Task> _actionExecute;
    }
}
