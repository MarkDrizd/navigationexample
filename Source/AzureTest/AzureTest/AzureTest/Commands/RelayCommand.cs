﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace AzureTest.Commands
{
    public class RelayCommand : BaseCommand
    {
        public RelayCommand(Action action, string message = null,
            [CallerMemberName] string commandName = "Unnamed command") : base(message)
        {
            _executeAction = action;
        }

        public override void Execute(object parameter)
        {
            try
            {
                _executeAction?.Invoke();
                SuccessCommand = true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                SuccessCommand = false;
            }
        }

        private readonly Action _executeAction;
    }
}
