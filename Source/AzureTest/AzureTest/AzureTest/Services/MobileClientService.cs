﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AzureTest.Models;
using Microsoft.WindowsAzure.MobileServices;
using MobileServiceClient = Microsoft.WindowsAzure.MobileServices.MobileServiceClient; 
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace AzureTest.Services
{
    public class MobileClientService : MobileServiceClient, IMobileClientService
    {
        public MobileClientService(string mobileAppUri) : base(mobileAppUri)
        {
        }

        public async Task InitLocalDatabaseAsync(string databaseName)
        {
            if (SyncContext.IsInitialized)
            {
                return;
            }

            SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;

            var localStore = new MobileServiceSQLiteStore(databaseName);

            var settings = SerializerSettings;

            var assemblyTypes = typeof(User).GetTypeInfo()
                .Assembly.DefinedTypes.Where(x => x.BaseType == typeof(BaseModel))
                .ToList();

            foreach (var typeInfo in assemblyTypes)
            {
                var type = typeInfo.AsType();
                var tableName = settings.ContractResolver.ResolveTableName(type);
                var contract = settings.ContractResolver.ResolveContract(type) as JsonObjectContract;

                if (contract == null)
                {
                    throw new ArgumentException("The generic type T is not an object.");
                }

                if (contract.DefaultCreator == null)
                {
                    throw new ArgumentException("The generic type T does not have parameterless constructor.");
                }

                //// create an empty object
                var theObject = contract.DefaultCreator();

                var item = ConvertToJObject(settings, theObject);

                //// set default values so serialized version can be used to infer types
                SetIdDefault(type, settings, item);
                SetNullDefault(contract, item);

                localStore.DefineTable(tableName, item);
            }

            await SyncContext.InitializeAsync(localStore);
        }

        public async Task<MobileServiceUser> SignInAsync(MobileServiceAuthenticationProvider provider, JObject token)
        {
            var user = await LoginAsync(provider, token);
            return user;
        }

        public async Task SignOutAsync()
        {
            await LogoutAsync();
        }

        private static JObject ConvertToJObject(JsonSerializerSettings settings, object theObject)
        {
            var jsonString = JsonConvert.SerializeObject(theObject, settings);
            var item = JsonConvert.DeserializeObject<JObject>(jsonString, settings);
            return item;
        }

        private static void SetIdDefault(Type type, MobileServiceJsonSerializerSettings settings, JObject item)
        {
            var idProperty = settings.ContractResolver.ResolveIdProperty(type);

            if (idProperty.PropertyType == typeof(long) || idProperty.PropertyType == typeof(int))
            {
                item[MobileServiceSystemColumns.Id] = 0;
            }
            else
            {
                item[MobileServiceSystemColumns.Id] = string.Empty;
            }
        }

        private static void SetNullDefault(JsonObjectContract contract, JObject item)
        {
            foreach (var itemProperty in item.Properties().Where(i => i.Value.Type == JTokenType.Null))
            {
                var contractProperty = contract.Properties[itemProperty.Name];

                if (contractProperty.PropertyType == typeof(string) || contractProperty.PropertyType == typeof(Uri))
                {
                    item[itemProperty.Name] = string.Empty;
                }
                else if (contractProperty.PropertyType == typeof(byte[]))
                {
                    item[itemProperty.Name] = new byte[0];
                }
                else if (contractProperty.PropertyType.GetTypeInfo().IsGenericType &&
                         contractProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    item[itemProperty.Name] = new JValue(
                        Activator.CreateInstance(contractProperty.PropertyType.GenericTypeArguments[0]));
                }
                else
                {
                    item[itemProperty.Name] = new JObject();
                }
            }
        }
    }
}
