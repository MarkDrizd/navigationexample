﻿using System;

namespace AzureTest.Services
{
    public interface IToastService
    {
        void ShowToast(string message, string title = null, string icon = null, int duration = 5, Action onTap = null, Action onDismiss = null);
    }
}
