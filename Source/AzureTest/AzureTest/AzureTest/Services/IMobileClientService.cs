﻿using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json.Linq;

namespace AzureTest.Services
{
    public interface IMobileClientService
    {
        Task InitLocalDatabaseAsync(string databaseName);
        Task<MobileServiceUser> SignInAsync(MobileServiceAuthenticationProvider provider, JObject token);
        Task SignOutAsync();
    }
}
