﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Autofac;
using AzureTest.Model;
using AzureTest.Models;
using AzureTest.PushHandlers;
using Microsoft.WindowsAzure.MobileServices;
using Xamarin.Forms;

namespace AzureTest.Services
{   //TODO: Send notifications by tag and user id
    public class PushNotificationService : IPushNotificationService
    {
  
        private readonly IMobileServiceClient _mobileServiceClient;
        private readonly IToastService _toastService;

        public PushNotificationService(IMobileServiceClient mobileClientService, IToastService toastService)
        {
            _mobileServiceClient = mobileClientService;
            _toastService = toastService;
        }

        public async Task RegisterUserForPushAsync()
        {
            // Wait for 5 seconds before asking for push permission on iOS...
            if (Device.RuntimePlatform == "iOS")
            {
                await Task.Delay(5000);
            } 

            //TODO: Add Allow push notifications user dialog

            // Wait for push registration
            do
            {
                await Task.Delay(1000);
            }
            while (string.IsNullOrEmpty(App.PushInstallationId));

            try
            {
                await _mobileServiceClient.InvokeApiAsync("Push/Register", HttpMethod.Post, new Dictionary<string, string> { { "installationId", App.PushInstallationId } });

                Debug.WriteLine("Push registration completed: InstallationId {0}", App.PushInstallationId);
            }
            catch (Exception ex)
            {
                if (ex is MobileServiceInvalidOperationException)
                {
                    var mobileServiceEx = (MobileServiceInvalidOperationException) ex;
                    await mobileServiceEx.Response.Content.ReadAsStringAsync();
                }

                Debug.WriteLine("Push registation failed:\n{0}", ex);
            }
        }

        public async Task UnRegister()
        {
            if (string.IsNullOrWhiteSpace(App.PushInstallationId))
            {
                return;
            }
            try
            {
                await _mobileServiceClient.InvokeApiAsync("Push/UnRegister", HttpMethod.Post,
                    new Dictionary<string, string> {{"pushInstallationId", App.PushInstallationId}});
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public bool ShouldCreateLocalNotification(PushNotification notification)
        {
            //Can be used for chat or other
            //Test purpose for now
            return true;
        }

        public void CreateLocalNotification(PushNotification notification)
        {
            _toastService.ShowToast(notification.Message, onTap: async () => { await NavigateToDetails(notification); });
        }

        private IPushNotificationHandler GetPushNotificationHandler()
        {
            var app = Application.Current as App;
            var allPushHandlers = app?.Container.Resolve<IEnumerable<IPushNotificationHandler>>();
            var pushNotificationHandler = allPushHandlers.FirstOrDefault();

            return pushNotificationHandler;
        }

        public void HandlePushNotification(PushNotification notification)
        {
            var pushNotificationHandler = GetPushNotificationHandler();
            pushNotificationHandler?.Handle(notification);
        }

        public async Task NavigateToDetails(PushNotification notification)
        {
            var pushNotificationHandler = GetPushNotificationHandler();

            if (pushNotificationHandler != null)
            {
                await pushNotificationHandler.NavigateToDetails(notification);
            }
        }

        public Task<PushNotification> SendTestPushNotification(PushNotification pushNotification, User toUser = null)
        {
            return _mobileServiceClient.InvokeApiAsync<PushNotification, PushNotification>("Push/SendTestPush", pushNotification,
                HttpMethod.Post, new Dictionary<string, string> { { "userId", "sid:6957559db23b1d91871cc19d9470e912" } });
        }
    }
}
