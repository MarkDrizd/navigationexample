﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using AzureTest.Enums;
using AzureTest.PageLocators;
using AzureTest.ViewModels.Base;
using AzureTest.Views.Base;
using Xamarin.Forms;

namespace AzureTest.Services
{
    public class NavigationService : INavigationService
    {
        protected INavigation Navigation { get; private set; }
        protected IPageLocator PageLocator { get; private set; }

        public NavigationService(IPageLocator pageLocator)
        {
            PageLocator = pageLocator;
        }

        public void SetMainViewModel<T>(object args = null) where T : IBaseViewModel
        {
            var page = ResolvePageFor<T>(args);

            if (page == null)
            {
                throw new Exception("Resolve page for " + typeof(T).Name + " returned null!");
            }

            var masterDetailPage = page as MasterDetailPage;

            if (masterDetailPage != null)
            {
                masterDetailPage.Detail = new BaseNavigationPage(masterDetailPage.Detail);
                Navigation = masterDetailPage.Detail.Navigation;
                Application.Current.MainPage = masterDetailPage;
            }
            else
            {
                var navigationPage = new BaseNavigationPage(page, false);
                Navigation = navigationPage.Navigation;
                Application.Current.MainPage = navigationPage;
            }
        }

        public async Task NavigateToAsync<T>(object args = null) where T : IBaseViewModel
        {
            var page = ResolvePageFor<T>(args);

            if (page == null)
            {
                var msg = args?.GetType() + " " + args;
                Debug.WriteLine(msg);
                return;
            }

            await Navigation.PushAsync(page);
        }

        public async Task NavigateToAsync<T1, T2>(object args1 = null, object args2 = null)
            where T1 : IBaseViewModel
            where T2 : IBaseViewModel
        {
            var page1 = ResolvePageFor<T1>(args1);
            var page2 = ResolvePageFor<T2>(args2);

            await Navigation.PushAsync(page1);

            Navigation.InsertPageBefore(page2, page1);
        }

        public async Task NavigateToAsync<T1, T2, T3>(object args1 = null, object args2 = null, object args3 = null)
            where T1 : IBaseViewModel
            where T2 : IBaseViewModel
            where T3 : IBaseViewModel
        {
            var page1 = ResolvePageFor<T1>(args1);
            var page2 = ResolvePageFor<T2>(args2);
            var page3 = ResolvePageFor<T3>(args3);

            await Navigation.PushAsync(page1);

            Navigation.InsertPageBefore(page2, page1);
            Navigation.InsertPageBefore(page3, page2);
        }

        public Task NavigateToModalAsync<T>(object args = null) where T : IBaseViewModel
        {
            var page = ResolvePageFor<T>(args);

            return Navigation.PushModalAsync(page);
        }

        public Task PopAsync()
        {
            return Navigation.PopAsync();
        }

        public Task PopModalAsync()
        {
            return Navigation.PopModalAsync();
        }

        public Task PopToRootAsync()
        {
            return Navigation.PopToRootAsync();
        }

        public Page ResolvePageFor<T>(object args = null) where T : IBaseViewModel
        {
            var page = PageLocator.ResolvePageAndViewModel(typeof(T), args);
            return page;
        }

        public void RemoveFromNavigationStack<T>(bool removeFirstOccurenceOnly = true) where T : IBaseViewModel
        {
            var pageType = typeof(T).IsAssignableTo<ITabbedViewModel>()
                ? typeof(BaseTabbedPage)
                : PageLocator.ResolvePageType(typeof(T));

            var navigationStack = Navigation.NavigationStack.Reverse();

            foreach (var page in navigationStack)
            {
                if (page.GetType() == pageType)
                {
                    Navigation.RemovePage(page);

                    if (removeFirstOccurenceOnly)
                    {
                        break;
                    }
                }
            }
        }

        public IReadOnlyList<IBaseViewModel> GetNavigationStack()
        {
            return Navigation.NavigationStack.Select(page => page.BindingContext as IBaseViewModel).ToList();
        }

        public void NotifyViewModel<T>(object sender, ViewModelNotificationType notificationType, object args = null)
            where T : IBaseViewModel
        {
            var navigationStack = GetNavigationStack().Reverse();
            foreach (var viewModel in navigationStack)
            {
                if (viewModel.GetType() == typeof(T))
                {
                    viewModel.OnNavigationServiceNotification(sender, notificationType, args);
                    break;
                }
                if (viewModel.GetType().IsAssignableTo<ITabbedViewModel>())
                {
                    var tabbedViewModel = (ITabbedViewModel) viewModel;
                    if (tabbedViewModel.ChildViewModels.Any(x => x.Value.GetType() == typeof(T)))
                    {
                        tabbedViewModel.ChildViewModels.First(x => x.Value.GetType() == typeof(T))
                            .Value.OnNavigationServiceNotification(sender, notificationType, args);
                        break;
                    }
                }
            }
        }

        public bool IsRootPage => Navigation.NavigationStack.Count == 1;

        public IBaseViewModel CurrentViewModel => CurrentPage?.BindingContext as IBaseViewModel;

        public Page CurrentPage => Navigation?.NavigationStack?.LastOrDefault();

        public void OpenDrawerMenu()
        {
            PresentDrawerMenu(true);
        }

        public void CloseDrawerMenu()
        {
            PresentDrawerMenu(false);
        }

        public void ToggleDrawerMenu()
        {
            var masterDetailPage = Application.Current.MainPage as MasterDetailPage;

            if (masterDetailPage != null)
            {
                masterDetailPage.IsPresented = !masterDetailPage.IsPresented;
            }
        }

        private void PresentDrawerMenu(bool isPresented)
        {
            var masterDetailPage = Application.Current.MainPage as MasterDetailPage;

            if (masterDetailPage != null)
            {
                masterDetailPage.IsPresented = isPresented;
            }
        }
    }
}
