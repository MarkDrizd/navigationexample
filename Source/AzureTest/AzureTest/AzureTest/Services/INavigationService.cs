﻿using AzureTest.Enums;
using AzureTest.ViewModels.Base;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AzureTest.Services
{
    public interface INavigationService
    {
        void SetMainViewModel<T>(object args = null) where T : IBaseViewModel;

        Task NavigateToAsync<T>(object args = null) where T : IBaseViewModel;

        Task NavigateToAsync<T1, T2>(object args1 = null, object args2 = null) where T1 : IBaseViewModel where T2 : IBaseViewModel;

        Task NavigateToAsync<T1, T2, T3>(object args1 = null, object args2 = null, object args3 = null) where T1 : IBaseViewModel where T2 : IBaseViewModel where T3 : IBaseViewModel;

        Task NavigateToModalAsync<T>(object args = null) where T : IBaseViewModel;

        Task PopAsync();

        Task PopModalAsync();

        Task PopToRootAsync();

        Page ResolvePageFor<T>(object args = null) where T : IBaseViewModel;

        void RemoveFromNavigationStack<T>(bool removeFirstOccurenceOnly = true) where T : IBaseViewModel;

        IReadOnlyList<IBaseViewModel> GetNavigationStack();

        void NotifyViewModel<T>(object sender, ViewModelNotificationType notificationType, object args = null) where T : IBaseViewModel;

        bool IsRootPage { get; }

        IBaseViewModel CurrentViewModel { get; }

        Page CurrentPage { get; }

        void OpenDrawerMenu();

        void CloseDrawerMenu();

        void ToggleDrawerMenu();
    }
}
