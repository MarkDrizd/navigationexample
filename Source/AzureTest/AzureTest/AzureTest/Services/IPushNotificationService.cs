﻿using System.Threading.Tasks;
using AzureTest.Model;
using AzureTest.Models;

namespace AzureTest.Services
{
    public interface IPushNotificationService
    {
        Task RegisterUserForPushAsync();

        Task UnRegister();

        void CreateLocalNotification(PushNotification notification);

        void HandlePushNotification(PushNotification notification);

        bool ShouldCreateLocalNotification(PushNotification notification);

        Task NavigateToDetails(PushNotification notification);

        Task<PushNotification> SendTestPushNotification(PushNotification pushNotification, User toUser = null);
    }
}
