﻿namespace AzureTest.Enums
{
    public enum ViewModelNotificationType
    {
        LocalRefresh = 0,
        ReSync,
        Pop,
        PopIfTop,
        LoginCompleted,
        CheckForProfilePictures
    }
}
