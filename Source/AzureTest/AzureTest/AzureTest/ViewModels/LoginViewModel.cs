﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using AzureTest.Commands;
using AzureTest.Services;
using AzureTest.ViewModels.Base;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json.Linq;

namespace AzureTest.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IMobileClientService _mobileClient;
        private readonly IPushNotificationService _pushNotificationService;

        public LoginViewModel(INavigationService navigationService, IMobileClientService mobileClient, IPushNotificationService pushNotificationService) : base(navigationService)
        {
            _mobileClient = mobileClient;
            _pushNotificationService = pushNotificationService;
        }

        public ICommand AfterGoogleLoginCommand
        {
            get
            {
                return new RelayCommandWithArgsAsync<object>(async args =>
                {
                    var accessToken = args as string;
                    await PerformLoginAsync(accessToken, MobileServiceAuthenticationProvider.Google);
                });
            }
        }

        public ICommand AfterFacebookLoginCommand
        {
            get
            {
                return new RelayCommandWithArgsAsync<object>(async args =>
                {
                    var accessToken = args as string;
                    await PerformLoginAsync(accessToken, MobileServiceAuthenticationProvider.Facebook);
                });
            }
        }

        public ICommand LoginErrorCommand
        {
            get
            {
                return new RelayCommandWithArgs<object>(args =>
                {
                    var ex = args as Exception ?? new Exception("Login error, but exception is null...");

                    throw new Exception(ex.Message);
                });
            }
        }

        private async Task PerformLoginAsync(string accessToken, MobileServiceAuthenticationProvider provider)
        {
            try
            {
                if (!string.IsNullOrEmpty(accessToken))
                {
                    JObject token = null;

                    if (provider == MobileServiceAuthenticationProvider.Facebook)
                    {
                        token = new JObject {{"access_token", accessToken}};
                    }

                    if (provider == MobileServiceAuthenticationProvider.Google)
                    {
                        token = new JObject {{"id_token", accessToken}};
                    }

                    await _mobileClient.SignInAsync(provider, token);

                    //TODO: Register notifications for specific user
                    await _pushNotificationService.RegisterUserForPushAsync();

                    NavigationService.SetMainViewModel<HomeViewModel>();
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
            }
        }
    }
}
