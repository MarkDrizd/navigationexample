﻿using AzureTest.Enums;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

namespace AzureTest.ViewModels.Base
{
    public interface IBaseViewModel : INotifyPropertyChanged
    {
        void Init(object args);

        void OnAppearing();

        void OnDisappearing();

        bool IsBusy { get; set; }

        string Title { get; set; }

        string Icon { get; set; }

        void OnNavigationServiceNotification(object sender, ViewModelNotificationType notificationType, object args);

        ObservableCollection<ToolbarItem> ToolbarItems { get; set; }

        Type PageType { get; }

        Type DrawerMenuViewModelType { get; }
    }
}
