﻿using AzureTest.Enums;
using AzureTest.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace AzureTest.ViewModels.Base
{
    public class BaseViewModel : AbstractNpcObject, IBaseViewModel
    {
        private object _initArgs;
        private bool _started;

        protected readonly INavigationService NavigationService;

        protected BaseViewModel(INavigationService navigationService)
        {
            NavigationService = navigationService;
            ToolbarItems = new ObservableCollection<ToolbarItem>();

            Title = string.Empty;
        }

        public bool IsVisible { get; private set; }

        public ITabbedViewModel ParentViewModel { get; set; }

        public bool IsBusy { get; set; }

        public string Title { get; set; }

        public string Icon { get; set; }

        public string Badge { get; set; }

        public virtual Type DrawerMenuViewModelType => null;

        public virtual Type PageType => null;

        public ObservableCollection<ToolbarItem> ToolbarItems { get; set; }

        public virtual void Init(object args)
        {
            _initArgs = args;
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null)
                return;

            base.OnPropertyChanged(propertyName);

            if (propertyName == nameof(Badge))
            {
                OnBadgeChange();
            }
        }

        public virtual void OnNavigationServiceNotification(object sender, ViewModelNotificationType notificationType, object args)
        {
            var data = new Dictionary<string, string>()
            {
                {"Sender", sender.GetType().Name},
                {"Receiver", GetType().Name},
            };

            Debug.WriteLine($"NavNotif {notificationType}", data);
        }

        public event Action<BaseViewModel> BadgeChange;

        public void OnBadgeChange()
        {
            BadgeChange?.Invoke(this);
        }

        public virtual void OnStart()
        {
            _started = true;
        }

        public virtual void OnAppearing()
        {
            IsVisible = true;

            if (!_started)
            {
                _started = true;
                OnStart();
            }
        }

        public virtual void OnDisappearing()
        {
            IsVisible = false;
        }
    }
}
