﻿using System;
using System.Collections.Generic;

namespace AzureTest.ViewModels.Base
{
    public interface ITabbedViewModel : IBaseViewModel
    {
        event EventHandler<ViewModelSelectionArgs> SelectedPageChange;

        IDictionary<string, BaseViewModel> ChildViewModels { get; set; }

        void SelectTab(Type viewModelType);

        void OnSelectedTabChanged(BaseViewModel selectedViewModel);
    }

    public class ViewModelSelectionArgs
    {
        public Type SelectedViewModelType { get; set; }
    }
}
