﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AzureTest.Models;
using AzureTest.Repositories;
using AzureTest.ViewModels.Base;
using AzureTest.Services;

namespace AzureTest.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private readonly IAzureRepository<User> _repository;

        public HomeViewModel(INavigationService navigationService, IAzureRepository<User> repository) : base(navigationService)
        {
            Title = "page title";
            _repository = repository;
        }

        public override async void Init(object args)
        {
            base.Init(args);

            Expression<Func<User, bool>> query = cl => cl.Deleted == false;
            var mobileItemsCollection = await _repository.GetItemsAsync(query, true);
            var items = mobileItemsCollection.ToList();

            //TODO: Display list on UI
        }
    } 
}
