﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace AzureTest.DAL.Models
{
    [JsonObject(IsReference = true)]
    public class User : BaseModel
    {
        [MaxLength(128)]
        [Index(IsUnique = true)]
        public string ExternalProviderUserId { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(128)]
        public string UserName { get; set; }

        [Index("IX_EmailAllowMultipleNulls", IsUnique = true)]
        [MaxLength(128)]
        [EmailAddress]
        public string Email { get; set; }
    }
}
