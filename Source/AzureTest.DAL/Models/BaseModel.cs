﻿using Microsoft.Azure.Mobile.Server;
using System;

namespace AzureTest.DAL.Models
{
    public abstract class BaseModel : EntityData
    {
        protected BaseModel()
        {
            Id = Guid.NewGuid().ToString("N");
        }
    }
}
