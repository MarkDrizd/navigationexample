using Microsoft.Azure.Mobile.Server.Tables;

namespace AzureTest.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public Configuration()
        {
            ContextKey = "AzureTest.DAL.DatabaseContext";
            SetSqlGenerator("System.Data.SqlClient", new EntityTableSqlGenerator());
        }

        protected override void Seed(DatabaseContext context)
        {
             
        }
    }
}
