﻿using System.Data.Entity;
using AzureTest.DAL.Models;
using Microsoft.Azure.Mobile.Server;

namespace AzureTest.DAL
{
    public class DatabaseContext : EntityContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Blog> Blogs { get; set; }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }
    }
}
